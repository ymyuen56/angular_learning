import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { ServersService } from '../servers.service';
import { CanComponentDeactive } from './can-deactivate-guid.service';

@Component({
  selector: 'app-edit-server',
  templateUrl: './edit-server.component.html',
  styleUrls: ['./edit-server.component.css']
})
export class EditServerComponent implements OnInit ,  CanComponentDeactive{
  server: {id: number, name: string, status: string};
  serverName = '';
  serverStatus = '';
  allowEdit = false;
  changeStatus = false;
  id = 1 ;

  constructor(
    private serversService: ServersService , 
    private route : ActivatedRoute ,
     private  router :Router) { 

  }

  ngOnInit() {
    console.log(this.route.snapshot.params);
    console.log(this.route.snapshot.queryParams);
    console.log(this.route.snapshot.fragment);

    this.route.fragment.subscribe();
    this.route.queryParams.subscribe((param : Params)=>{
      this.allowEdit = param['allowEdit'] === '1' ? true : false  
    });
    this.id = this.route.snapshot.params['id']
    this.route.params.subscribe((param : Params) =>{
      this.id = Number(param['id'])
    })
    this.server = this.serversService.getServer(this.id);
    this.serverName = this.server.name;
    this.serverStatus = this.server.status;
  }

  onUpdateServer() {
    this.serversService.updateServer(this.server.id, {name: this.serverName, status: this.serverStatus});
    this.changeStatus = true
    this.router.navigate(["../"] , { relativeTo: this.route})
  }

  canDeactivate() : boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if (!this.allowEdit){
      return true
    }
    if ((this.serverName != this.server.name || this.serverStatus != this.server.status) && !this.changeStatus){
      return confirm("Do you want to discard the changes?")
    }
    return true
  }
}
