import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./auth-guard.service";
import { HomeComponent } from "./home/home.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { CanDeactivateGuard } from "./servers/edit-server/can-deactivate-guid.service";
import { EditServerComponent } from "./servers/edit-server/edit-server.component";
import { ServerResolver } from "./servers/server/server-resolve.service";
import { ServerComponent } from "./servers/server/server.component";
import { ServersComponent } from "./servers/servers.component";
import { UserComponent } from "./users/user/user.component";
import { UsersComponent } from "./users/users.component";

const appRoutes: Routes = [
    { path : '' , component: HomeComponent},
    { path : 'users' , component: UsersComponent , children:[
      { path : ':id/:name' , component: UserComponent},
    ]},
    { path : 'servers', canActivateChild:[AuthGuard],component: ServersComponent , children:[
      { path : ':id', component: ServerComponent , resolve: {  server : ServerResolver}},
      { path : ':id/edit' , component: EditServerComponent , canDeactivate:[CanDeactivateGuard]} , 
    ]},
    { path : 'not-found' , component: PageNotFoundComponent , data:{message :"Page not found" }},
    { path : '**', redirectTo:'/not-found'}
    /*
      Since the default matching strategy is "prefix" , Angular checks if the path you entered in the URL does start with the path specified in the route. Of course every path starts with ''  (Important: That's no whitespace, it's simply "nothing").
  
  To fix this behavior, you need to change the matching strategy to "full" :
  
  { path: '', redirectTo: '/somewhere-else', pathMatch: 'full' } 
    
    */
  ]


@NgModule({
    //imports: [RouterModule.forRoot(appRoutes, {useHash: true})],
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRouteModule {}