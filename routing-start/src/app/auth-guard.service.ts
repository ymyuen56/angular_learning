import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree
} from '@angular/router'
import { Observable } from 'rxjs';
import { AuthService } from './auth.service'

@Injectable()
export class AuthGuard implements CanActivate , CanActivateChild{
    constructor(private authService : AuthService , private route :Router){

    }
    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        return this.canActivate(childRoute,state)
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        return this.authService.isAuthenticated().then(
            (isAuth : boolean)=>{
                if(isAuth){
                    return true
                }else {
                    this.route.navigate([''])
                }
            }
        )
    }

}