import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, RouterEvent } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit , OnDestroy{
  user: {id: number, name: string};
  paramsSubscription : Subscription

  constructor(private route :ActivatedRoute) { }

  ngOnInit() {
    this.user = {
      id : this.route.snapshot.params['id'],
      name : this.route.snapshot.params['name']
    }
    //this would not reload it as long as you are on the component 
    
    this.paramsSubscription = this.route.params.subscribe((param : Params) =>{
      this.user.id =  param['id']
      this.user.name = param['name']
    })

    // subscribe <- will hold on memory even though the component destory
    // However , Angular will do it for you .

  }

  ngOnDestroy(){
    this.paramsSubscription.unsubscribe()
  }
}
